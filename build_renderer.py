#! /usr/bin/env python3
import argparse

from cffi import FFI

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--debug", action="store_true", help="disable optimisation")
    args = parser.parse_args()
    if args.debug:
        c_args = ["-O0"]
    else:
        c_args = ["-fopenmp"]

    ffibuilder = FFI()
    ffibuilder.set_source("_renderer", '#include "renderer.c"', extra_compile_args=c_args, extra_link_args=["-fopenmp"], libraries=["m"])
    ffibuilder.cdef("""
bool render(unsigned char* buff, size_t width, size_t height, size_t depth, size_t iterations, int mode, size_t count);
    """)

    ffibuilder.compile(verbose=True)
