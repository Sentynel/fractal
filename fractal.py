#! /usr/bin/env python3
import argparse

from PIL import Image

from _renderer import ffi, lib as renderer

modes = {
        "escape": 0,
        # "escape-smooth": 1, # not implemented
        "buddha": 2,
        "nebula": 3,
        }

def render(width, height, mode, iterations, count):
    if mode == "buddha":
        depth = 1
    else:
        depth = 3
    buff = ffi.new("unsigned char[]", width*height*depth)
    m = modes[mode]
    if not renderer.render(buff, width, height, depth, iterations, m, count):
        raise ValueError("rendering failed")
    if mode == "buddha":
        mi = "L"
    else:
        mi = "RGB"
    im = Image.frombytes(mi, (width, height), ffi.buffer(buff))
    return im

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("width", type=int, help="width of output image")
    parser.add_argument("height", type=int, help="height of output image")
    parser.add_argument("-m", "--mode", choices=modes, help="render mode", default="escape")
    parser.add_argument("-o", "--output", default="fractal.png", help="output file name")
    parser.add_argument("-i", "--iterations", default=100, type=int, help="maximum iterations (default 100)")
    parser.add_argument("-c", "--count", default=None, type=int, help="number of points to render (default 100 per pixel)")
    args = parser.parse_args()

    if args.count is None:
        args.count = args.width * args.height * 100;

    im = render(args.width, args.height, args.mode, args.iterations, args.count)
    im.save(args.output)
