#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>

typedef struct {
    size_t width;
    size_t height;
    size_t depth;
    unsigned char* buff;
    size_t iterations;
    double x_scale;
    double y_scale;
} buffer;

typedef struct {
    double x;
    double y;
} point;

const float x_min = -2.5;
const float x_max = 1;
const float y_min = -1;
const float y_max = 1;

size_t iter(buffer* b, point* traj, double x0, double y0)
{
    double x=0, y=0, xt;//, q;
    size_t i=0;

    // quick bulb checks
    // for some reason this breaks the buddhabrot
    /*
    q = pow(x0 - 0.25, 2) + y0 * y0;
    if ((q * (q + (x0 - 0.25))) >= (0.25 * y0 * y0))
        return b->iterations;
    if ((pow(x0 + 1, 2) + y0 * y0) >= (1./16))
        return b->iterations;
    */
    // technically the max coord we can actually see is 2.5,1 so we make the
    // cap (squared) 8, not the traditional 4
    while (((x*x + y*y) < 8) && (i < b->iterations))
    {
        xt = x*x - y*y + x0;
        y = 2*x*y + y0;
        x = xt;
        if (traj)
        {
            traj[i].x = x;
            traj[i].y = y;
        }
        i++;
    }
    return i;
}

void escape_color(buffer* buff, size_t w, size_t h, size_t count)
{
    size_t offset;
    unsigned char rg, b;
    if (count == buff->iterations)
        // leave black
        return;
    offset = (h * buff->width + w) * buff->depth;
    // write RGB value
    rg = 255 * ((double)count / buff->iterations);
    b = 128 + 127 * ((double)count / buff->iterations);
    buff->buff[offset] = rg;
    buff->buff[offset+1] = rg;
    buff->buff[offset+2] = b;
}

bool escape(buffer* b)
{
    size_t w, h, count;
    double x0, y0;
    for (w=0; w < b->width; w++)
    {
        for (h=0; h < b->height; h++)
        {
            x0 = w * b->x_scale + x_min;
            y0 = h * b->y_scale + y_min;
            count = iter(b, NULL, x0, y0);
            escape_color(b, w, h, count);
        }
    }
    return true;
}

void random_point(buffer* b, point* p)
{
    p->x = x_min + (random() / (RAND_MAX / (x_max - x_min)));
    p->y = y_min + (random() / (RAND_MAX / (y_max - y_min)));
}

size_t get_bucket(buffer* b, point* traj, size_t i)
{
    size_t x, y;
    if ((traj[i].x < x_min) || (traj[i].x > x_max) || (traj[i].y < y_min) || (traj[i].y > y_max))
        return (size_t)-1;
    x = (traj[i].x - x_min) / b->x_scale;
    y = (traj[i].y - y_min) / b->y_scale;
    return y * b->width + x;
}

void buddha_heatmap(buffer* b, size_t* map, point* traj, size_t count)
{
    size_t offset, i;
    for (i=0; i<count; i++)
    {
        offset = get_bucket(b, traj, i);
        if (offset == (size_t)-1)
            continue;
#pragma omp atomic
        map[offset]++;
    }
}

size_t buddha_normalise(size_t* map, size_t len)
{
    size_t i, x, k=0, cutoff;
    double A=0, A_1=0, Q=0, stdev;
    for (i=0; i<len; i++)
    {
        k = i+1;
        x = map[i];
        A_1 = A;
        A = A + (x - A) / k;
        Q = Q + (x - A_1) * (x - A);
    }
    stdev = sqrt(Q / k) * 3;
    cutoff = A + stdev;
    for (i=0; i<len; i++)
    {
        if (map[i] > cutoff)
            map[i] = cutoff;
    }
    return cutoff;
}

void buddha_color(buffer* b, size_t* map, size_t max, size_t offset)
{
    size_t i;
    unsigned char c;
    for (i=0; i < (b->height * b->width); i++)
    {
        c = (255 * map[i]) / max;
        b->buff[i * b->depth + offset] = c;
    }
}

bool buddha(buffer* b, size_t count, size_t offset)
{
    size_t i = 0, tlen = 0, max = 0;
    point p = {0};
    point *traj = NULL;
    size_t *map = NULL;
    bool succeeded = true;
    map = malloc(sizeof(size_t) * b->width * b->height);
    if (!map)
        return false;
    memset(map, 0, sizeof(size_t) * b->width * b->height);
#pragma omp parallel shared(map, succeeded) private(i, tlen, p, traj)
    { do {
        traj = malloc(sizeof(point) * b->iterations);
        if (!traj)
        {
            succeeded = false;
            break;
        }

#pragma omp for
        for (i=0; i<count; i++)
        {
            random_point(b, &p);
            tlen = iter(b, traj, p.x, p.y);
            if (tlen != b->iterations)
                buddha_heatmap(b, map, traj, tlen);
        }

        free(traj);
    } while (false); }
    max = buddha_normalise(map, b->width * b->height);
    buddha_color(b, map, max, offset);
    free(map);
    return succeeded;
}

bool nebula(buffer* b, size_t count)
{
    // basically we want to do three renders at one byte offsets, with the
    // iteration cap increasing by *10 each time
    size_t i;
    for (i=2; i<3; i--)
    {
        if (!buddha(b, count, i))
            return false;
        printf("render pass done at %li\n", time(NULL));
        b->iterations *= 10;
    }
    return true;
}

void init_buffer(buffer* b)
{
    b->x_scale = (x_max - x_min) / b->width;
    b->y_scale = (y_max - y_min) / b->height;
}

bool render(unsigned char* buff, size_t width, size_t height, size_t depth, size_t iterations, int mode, size_t count)
{
    buffer b = {width, height, depth, buff, iterations};
    init_buffer(&b);
    if (mode == 0)
        return escape(&b);
    if (mode == 2)
        return buddha(&b, count, 0);
    if (mode == 3)
        return nebula(&b, count);
    return false;
}

/*
// for testing
int main()
{
    unsigned char* buff;
    size_t width=300, height=300, depth=3, iterations=100, count=900000;
    int mode=3;
    buff = malloc(width * height * depth);
    render(buff, width, height, depth, iterations, mode, count);
    free(buff);
    return 0;
}
*/
